import sys

def main():
    path = sys.argv[1]
    with open(path) as f:
        print(f"--- Begin report of {path} ---")
        content = f.read()
        word_count = count_words(content)
        print(f'{word_count} words were found in the document\n')
        letter_dict = count_letters(content)
        sorted_list = dict_to_sorted_list(letter_dict)
        print_letters_report(sorted_list)
        print("--- End report ---")

def count_words(string):
    words = string.split()
    return len(words)    

def count_letters(string):
    count_dict = {}
    for char in string:
        lower = char.lower()
        if lower not in count_dict:
            count_dict[lower] = 1
        else:
            count_dict[lower] += 1
    return count_dict

def print_letters_report(list):
    for dict in list:
        print(f"The '{dict['char']}' character was found {dict['count']} times")

def dict_to_sorted_list(dict):
    list = []
    for key in dict:
        if key.isalpha():
            list.append({"char": key, "count": dict[key] })
    list.sort(reverse=True, key=sort_by_count)
    return list

def sort_by_count(dict):
    return dict["count"]

main()